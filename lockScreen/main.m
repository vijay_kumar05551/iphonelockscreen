//
//  main.m
//  lockScreen
//
//  Created by cli-macmini-18 on 1/11/16.
//  Copyright © 2016 cli-macmini-18. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
